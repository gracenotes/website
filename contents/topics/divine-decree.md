---
title: "The Divine Decrees"
pdf: "divinedecree.pdf"
description: "The classic definition comes from R.B. Thieme, Jr.: “The decree of God is His eternal (always existed), holy (perfect integrity), wise (the application of omniscience to creation), and sovereign purpose, comprehending simultaneously all things that ever were or will be in their causes, conditions (status), successions (interaction with others that leads to certain decisions), relations, and determining their certain futurition.”"
tableOfContents: false
template: topic.jade
---


